# 1. Написать Ansible роль, устанавливающую PostgreSQL.
# 2. Написать playbook, который будет применять роль к группе [db]. Настройки брать из group_vars.
# 3. Все параметры вынести в переопределяемые переменные.
# 4. Протестировать роль с помощью molecule.
# 5. Настроить pipeline для автоматической проверки роли.